resource "aws_instance" "ec2_test" {

  ami = "ami-08ca3fed11864d6bb"
  instance_type = "t2.micro"
  security_groups = [aws_security_group.SG_public_test.id]
  count = 1
  associate_public_ip_address = true
    subnet_id = aws_subnet.subnet_public_test.id
  key_name = "donono"

  tags = {
    Name = "ec2_test"
  }

}