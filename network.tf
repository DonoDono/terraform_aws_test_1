resource "aws_vpc" "vpc_test" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name ="VPC_test"
  }
}

resource "aws_subnet" "subnet_public_test" {
  vpc_id = aws_vpc.vpc_test.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name= "Subnet_public_test"
  }
}

resource "aws_internet_gateway" "IGW_test" {
  vpc_id = aws_vpc.vpc_test.id

  tags = {
    Name = "igw_test"
  }
}

resource "aws_security_group" "SG_public_test" {
  name="SG_public_test"
  vpc_id = aws_vpc.vpc_test.id
  description = "allow ssh"

  ingress {
    from_port = 22
    protocol  = "tcp"
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol  = "-1"
    to_port   = 0
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name ="SG_public_test"
  }

}

resource "aws_route_table" "route_table_public_test" {
  vpc_id = aws_vpc.vpc_test.id

  route {
    gateway_id = aws_internet_gateway.IGW_test.id
    cidr_block = "0.0.0.0/0"
  }

  tags = {
    Name = "route_table_public_test"
  }
}

resource "aws_route_table_association" "rt_public_association" {
  route_table_id = aws_route_table.route_table_public_test.id
  subnet_id = aws_subnet.subnet_public_test.id
}


